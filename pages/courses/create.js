import { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';


export default function create() {

    const [courseId, setCourseId] = useState('')
    const [courseName, setCourseName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
   

    function createCourse(e){
        e.preventDefault();

        console.log(`${courseName} is set to start on ${startDate} for PHP ${price} per slot.`)
        
        setCourseId('');
        setCourseName('');
        setDescription('');
        setPrice(0);
        setStartDate('');
        setEndDate('');
        
    }


    return (
        
        <Container>
        	<Form onSubmit={(e) => createCourse(e)}>
            <Form.Group controlId="courseId">
                <Form.Label>Course ID</Form.Label>
                <Form.Control type="" placeholder="Enter course ID" value={courseId} onChange={e => setCourseId(e.target.value)} required/>
                <Form.Text className="text-muted">
                dlkjdslfj
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="courseName">
                <Form.Label>courseName</Form.Label>
                <Form.Control type="courseName" placeholder="courseName" value={courseName} onChange={e => setCourseName(e.target.value)} required/>
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control type="description" placeholder="description" value={description} onChange={e => setDescription(e.target.value)} required/>
            </Form.Group>

            <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control type="price" placeholder="price" value={price} onChange={e => setPrice(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="startDate">
                <Form.Label>Start Date</Form.Label>
                <Form.Control type="startDate" placeholder="startDate" value={startDate} onChange={e => setStartDate(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="endDate">
                <Form.Label>End Date</Form.Label>
                <Form.Control type="endDate" placeholder="endDate" value={endDate} onChange={e => setEndDate(e.target.value)} required/>
            </Form.Group>

            
                <Button variant="primary" type="submit" id="submitBtn">Submit</Button> 
            
        </Form>
        </Container>
    )
}